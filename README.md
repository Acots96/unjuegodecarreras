# PEC1


Este es un juego de coches de carreras, con el típico modo de contrareloj. Para jugar únicamente hay que utilizar las flechas de selección del teclado para mover el coche, y seleccionar un coche y un circuito en el menú principal.


Consta de 2 tipos de escenas:
 - Menu: La escena del menú, en la que se puede seleccionar el coche y el circuito.
 - RoadX: Escenas de circuitos, cada una con un circuito diferente.
 
 
Los escenarios de circuitos han sido montados utilizando la potente herramienta Terrain que ofrece Unity para modelar terrenos, el asset EasyRoads para generar la carretera y finalmente un sencillo asset de edificios para edificar parte del terreno.


Cada circuito está compuesto de 3 vueltas. Para poder comprobar que el coche ha dado una vuelta han sido colocados unos checkpoints invisibles por los que este debe pasar para poder completar el circuito. Todos los checkpoints están desactivados al inicio excepto uno (el primero que debe cruzar el jugador), el cual activará el siguiente y así sucesivamente hasta activar la meta. El objetivo de este sistema es asegurarse de que el jugador no trata de terminar la carrera yendo hacia atrás, sino solo completando el circuito.
Además, el circuito tiene elementos a los lados que dificultan o imposibilitan el poder ir por fuera del circuito para llegar antes a la meta. Dichos elementos son edificios en algunas zonas que no permiten el paso, y por otro lado salirse de la carretera conlleva una penalización, que es la reducción de la velocidad del coche.
Para reducir la velocidad del coche al sairse de la carretera se ha utilizado un RaycastHit para cada rueda, el cual comprueba los layers de los colliders que hay debajo de la misma y, en el caso de que no encuentre el layer que pertenece a la carretera, hace que la velocidad límite del coche disminuya drásticamente. El RaycastHit es un método que consume una gran cantidad de recursos, por lo que no se hacen las 4 ruedas cada frame, sino que se utiliza el método InvokeRepeating para que se haga el RaycastHit cada cierto tiempo, de esta forma no se consumen tantos recursos y visualmente no se aprecia la diferencia.


Para realizar el coche fantasma, se ha utilizado una copia de la instancia del coche de Unity con los colores de parte de sus materiales modificados para simular transparencia, y así lograr un verdadero efecto de fantasma.
Este coche fantasma solo es visible en el caso de que ese circuito ya haya sido realizado por lo menos una vez, ya que de lo contrario no existen datos guardados. 
Para poder guardar los datos de la mejor vuelta se ha utilizado una clase serializable que contiene 3 atributos necesarios: las listas de posiciones y rotaciones, y el tiempo que se ha tardado en realizar la vuelta para poder comparar con otras vueltas de ese circuito y guardar la que sea mejor. Dichos datos se guardan en un fichero JSON, que contiene la mejor vuelta de cada circuito.
Se hace uso de una corutina que almacena en dos listas (posiciones y rotaciones) los datos del coche cada cierto tiempo, ya que hacerlo cada frame generaría una cantidad de datos inecesariamente grande. Al terminar una vuelta, se compara el tiempo con la mejor vuelta guardada en el JSON y, en el caso de ser menor (o ser la primera), se actualizan los datos del JSON con la nueva mejor vuelta. 
Por otro lado, para utilizarlos con el coche fantasma, se leen del JSON y se utiliza otra corutina que actualiza la posición y la rotación cada cierto tiempo (mismo intervalo de tiempo que el utilizado al guardar los datos). Para evitar que el movimiento de este coche sea demasiado brusco, la corutina utiliza ese intervalo de tiempo entre dos actualizaciones de posición y rotación para obtener una posición y rotación media dependiendo del tiempo transcurrido, de esta forma se consigue un conjunto de <posición,rotacion> bastante preciso para todos los frames del juego.


Para poder ver la repetición del circuito recién realizado, se ha reciclado la implementación utilizada para el coche fantasma ya que la funcionalidad es la misma, pero utilizando otras listas para guardar los datos, ya que su finalidad es distinta a las que contienen la información del coche fantasma.
Además, para simular una repetición como las que se pueden ver por televisión, se ha implementado un algoritmo que, de forma aleatoria cada cierto tiempo, elige entre:
 - Perseguir al coche con la cámara, pero desde un ángulo distinto al de la carrera.
 - Poner cámaras fijas que persiguen al coche como si de un espectador sentado en las gradas se tratara.
Para poder llevar a cabo ambos métodos se ha utilizado el asset Cinemachine de Unity, el cual ofrece una cantidad de posibilidades inmensa de manipulación de cámaras. 
Ambos métodos utilizan el atributo LookAt de CinemachineVirtualCamera para perseguir al coche con la "mirada", pero ambos difieren al utilizar el atributo Follow. Para perseguir al coche como si la cámara se moviese a la misma velocidad (también utilizado durante la carrera detrás del coche) hace falta especificarle un Transform al Follow que es el que tratará de perseguir. En este caso, para hacer que lo persigua desde un ángulo distinto, el Transform utilizado ha sido uno que forma parte del coche, pero con una rotación distinta, por lo que la cámara nunca queda justo detrás del coche. 
Por otro lado, para hacer que la cámara solo persiga al coche con la mirada, el atributo Follow debe quedar nulo, solo especificando el LookAt. Además, para simular un conjunto de cámaras que miran el coche (una tras otra), ha sido utilizada una lista de posiciones elevadas por todo el circuito y la constante comprobación de qué cámara está maás cerca del coche (si la actual o la siguiente en la lista).


Para que el jugador pueda tener información sobre los tiempos de cada vuelta, se ha implementado una UI cuidada que muestra el tiempo de cada vuelta en la esquina superior derecha, además del tiempo actual de la vuelta en la perte superior central y, además, eltiempo de la mejor vuelta en ese circuito en la parte derecha de la pantalla.
A cada vuelta completada se actualiza el tiempo de la misma en el correspondiente texto de la parte superior izquierda. A cada vuelta también se comprueba si el tiempo es mejor que el de la mejor vuelta, y de ser así también se actualiza el correspondiente texto.
Todos los tiempos mostrados tienen el formato "--:--.---", donde los dos primeros números son los minutos, los dos del medio son los segundos y los tres últimos son las milésimas de segundo.


En el menú de selección se puede elegir entre 3 coches distintos y 2 circuitos distintos, cuya información está guardada en un JSON al que se accede mediante Resources.
 - Cada coche tiene 4 valores que cambian (máximo giro del coche, potencia sobre las ruedas, fuerza de agarre y velocidad máxima), el nombre, el path de una foto de muestra del coche y 3 colores aplicados a varios materiales distintos para hacer a los coches visualmente distintos entre ellos.
 - El archivo JSON también tiene información acerca de los circuitos disponibles, que consiste en el nombre del circuito y el path de la imagen (vista desde arriba) del circuito.


Para simular daño visible al coche, se le han añadido varios puntos distribuídos por su carrocería que contienen efectos de partículas con humo vertical, los cuales se activan aleatoriamente según la cantidad de daño que haya recibido el coche. Para calcular esa cantidad de daño se comprueba cada colisión del coche con los colliders estáticos, de las que se extrae la cantidad de impulso generado en el momento del impacto para saber la fuerza con la que ha impactado el coche. Ese valor se resta de un nivel de vida que tiene el coche desde el inicio, y el resultado se compara con un umbral de nivel de daños.
Dicho umbral representa el límite que puede soportar el coche sin que los daños sean apreciables, pero una vez superado dicho umbral los daños apreciables serán proporcionales al nivel de vida que le quede al coche. Si por ejemplo el umbral está al 80%, y el coche tiene 10 puntos de humo, todo el daño recibido hasta que la vida llegue al 80% no afectará, pero cuando baje de ese 80% los puntos de humo se irán activando cada (80/10=) 8% hasta llegar a 0, donde el coche habrá sufrido el máximo daño posible. Este no explotará ni se quedará parado, pero los daños serán muy visibles y la velocidad límite (que también irá disminuyendo proporcionalmente) se verá seriamente afectada.
Además, los impactos del coche tienen un efecto de sonido según la intensidad de este.


Para darle más vida al juego, se han añadido una serie de canciones que se reproducen aleatoriamente, una seguida de la otra, tanto en el menú principal como en las escenas de los circuitos. Además, se utilizan efectos de sonido como el de un golpe, reproducido cada vez que el coche choca contra algún objeto (con más o menos volumen según la fuerza de impacto). Otros efectos de sonido implementados han sido el del motor del coche, el cual utiliza el pitch según la aceleración y la velocidad de este para simular el ruído del motor y del cambio de marchas, y el sonido de la marcha atrás inicial (un ligero pitido). Finalmente, en la escena del menú, se ha implementado el efecto de sonido de los botones "anterior" y "siguiente" de la selección de coche y circuito.


Para simular atajos en los circuitos ha sido necesario montarlos para que el jugador pueda pasar de un punto a otro del circuito sin tener que hacer ese tramo completo. Para ello, una o más zonas no contienen restricciones de paso como edificios o bosques muy densos, sino que hay trozos de terreno libres de elementos que permiten acortar el circuito. Dichos tramos de circuito no contienen checkpoints ya que el jugador puede no pasar por ahí. La única restricción está en que, al no ser carretera, el coche irá más lento hasta que llegue de nuevo a la carretera, pero es lo suficientemente corto como para beneficiar al que pase por ahí.


Scripts:
 - DataManager: Gestiona los datos guardados en Resources, como los JSON o las imágenes, y los actualiza cuando es necesario.
 - DataManager.LapData: Guarda la información de una vuelta (útil para usarlo con el coche fantasma y para la repetición de la carrera).
 - MenuManager: Carga los datos de los coches y los circuitos, y los muestra por pantalla para que el usuario pueda elegir.
 - GameManager: Encargado de la gestión de la escena del circuito y del "loop" princiapl: Lleva el control de las vueltas realizadas, muestra la cuenta atrás inicial y va actualizando el tiempo de las vueltas en pantalla, va recopilando los datos del fantasma y de la mejor vuelta para pasárselos a DataManager y finalmente se encarga de avisar a PECCarController para que inicie la repetición.
 - CarController: Controla el movimiento del coche, contiene los datos modificables (guardados en el JSON) y también controla cuando este está en el suelo (mediante RaycastHit en las ruedas).
 - PECCarController: Controla algunas de las funcionalidades del coche de esta PEC, como la repetición mediante el juego de cámaras, la actualización los datos del coche según la elección del jugador, los efectos de sonido del motor del coche y de los golpes, y finalmente los efectos visuales de daño.
 - CheckPointController: Controla cuando el coche pasa por uno de los checkpoints e informa a GameManager.

(Para conocer la implementación con más detalle, ver los comentarios y el código de las scripts descritos previamente)


Enlace al video explicativo de la PEC1: https://youtu.be/lhy-7RebBRM
