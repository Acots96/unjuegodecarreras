﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/**
 * Clase anteriormente utilizada para guardar 
 * la informacion del fantasma mediante ScriptableObject,
 * actualmente en desuso al haber implementado una forma propia
 * mediante el uso de JSON.
 */

//[CreateAssetMenu]
public class GhostLapData {

    //[SerializeField] private float time;
    //[SerializeField] private List<Vector3> carPositions;
    //[SerializeField] private List<Quaternion> carRotations;
    [SerializeField] private List<Vector3> carPositions;
    [SerializeField] private List<Quaternion> carRotations;

    public float Time;


    private void AddNewData(Transform tr) {
        carPositions.Add(tr.position);
        carRotations.Add(tr.rotation);
    }
    public void AddFullData(List<Vector3> positions, List<Quaternion> rotations, float t) {
        Time = t;
        carPositions.AddRange(positions);
        carRotations.AddRange(rotations);
        //DataManager.StoreGhostLapData("ghost_lap.txt")
    }

    public void GetDataAt(int sample, out Vector3 position, out Quaternion rotation) {
        int s = sample == carPositions.Count ? carPositions.Count - 1 : sample;
        position = carPositions[s];
        rotation = carRotations[s];
    }


    public bool IsEmpty() {
        return carPositions.Count == 0;
    }

    public void Reset() {
        carPositions.Clear();
        carRotations.Clear();
    }
}

