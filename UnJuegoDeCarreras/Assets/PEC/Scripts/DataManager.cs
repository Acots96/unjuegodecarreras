﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public static class DataManager {

    private static string PEC1DATA_FILENAME = "pec1_data";
    private static string GHOSTLAPDATA1_FILENAME = "ghost_lap1";
    private static string GHOSTLAPDATA2_FILENAME = "ghost_lap2";

    ///

    private static PEC1Data data;


    public static void LoadMenuData() {
        string json = Resources.Load<TextAsset>(PEC1DATA_FILENAME).text;
        data = JsonUtility.FromJson<PEC1Data>(json);
    }

    public static void LoadData(bool isRoad1) {
        LoadGhostLapData(isRoad1);
    }


    public static List<CarData> GetCarsData() {
        return data.carsList;
    }

    public static List<RoadData> GetRoadsData() {
        return data.roadsList;
    }


    /**
     * Clases y structs Serializables para poder leer y guardar datos del JSON
     * que guarda las informaciones de los coches y las carreteras
     * (informacion para el menu)
     */

    [Serializable]
    private class PEC1Data {
        public List<CarData> carsList;
        public List<RoadData> roadsList;
    }

    [Serializable]
    public struct CarData {
        public string carname;
        public string maximumsteerangle;
        public string fulltorqueoverallwheels;
        public string downforce;
        public string topspeed;
        public string imagepath;
        public Materials materials;
    }

    [Serializable]
    public class Materials {
        public MaterialData body;
        public MaterialData components;
        public MaterialData muds;
        public MaterialData wheels;
    }

    [Serializable]
    public struct MaterialData {
        public string color;
        public string metallic;
        public string smoothness;
    }

    [Serializable]
    public struct RoadData {
        public string roadname;
        public string imagepath;
    }




    ///////////////////////////////////////////////


    public static LapData GhostLapData { get; private set; }


    /**
     * Carga la vuelta fantasma de la escena en la que este, solo si existe el fichero,
     * sino sera creada en la siguiente vuelta por el metodo siguiente.
     */
    private static void LoadGhostLapData(bool isRoad1) {
        TextAsset asset = Resources.Load<TextAsset>(isRoad1 ? GHOSTLAPDATA1_FILENAME : GHOSTLAPDATA2_FILENAME);
        if (asset != null && !asset.text.Equals("")) {
            GhostLapData = JsonUtility.FromJson<LapData>(asset.text);
        }
    }

    /**
     * Guarda los datos de la mejor vuelta, creando el fichero si este no existe.
     * Dado que la carpeta Resources esta en sitios distintos en el caso de ejecutar el juego
     * desde el editor o desde una build, lo tiene en cuenta y usa el path correspondiente.
     */
    private static void StoreGhostLapData(bool isRoad1) {
        string json = JsonUtility.ToJson(GhostLapData);
        File.WriteAllText(GetResourcesPath()+(isRoad1 ? GHOSTLAPDATA1_FILENAME : GHOSTLAPDATA2_FILENAME)+".json", json);
        if (Application.isEditor)
            UnityEditor.AssetDatabase.Refresh();
    }
    private static string GetResourcesPath() {
        string path = Application.dataPath+"/";
        if (Application.isEditor)
            path += "PEC/";
        path += "Resources/";
        return path;
    }


    public static bool IsGhostLapDataEmpty() {
        return GhostLapData == null || GhostLapData.Time == 0;
    }

    public static float GetGhostLapTime() {
        return GhostLapData.Time;
    }


    /**
     * Metodo para guardar los datos de la vuelta fantasma, siempre que exista,
     * y teniendo en cuenta la escena de circuito.
     */
    public static void SetGhostLapData(LapData lap, bool isRoad1) {
        if (GhostLapData == null)
            GhostLapData = new LapData();
        else
            GhostLapData.Reset();
        GhostLapData.AddFullData(lap);
        StoreGhostLapData(isRoad1);
    }




    /**
     * Clase serializable para guardar los datos de una vuelta fantasma.
     * Al ser la clase utilizada para generar los datos, ademas de para 
     * el coche fantasma tambien se utiliza para guaradar la informacion 
     * de las 3 vueltas del jugador.
     */

    [Serializable]
    public class LapData {

        public float Time;
        public List<Vector3> carPositions;
        public List<Quaternion> carRotations;

        public LapData() {
            Time = 0;
            carPositions = new List<Vector3>();
            carRotations = new List<Quaternion>();
        }

        public void GetDataAt(int sample, out Vector3 position, out Quaternion rotation) {
            int s = sample == carPositions.Count ? carPositions.Count - 1 : sample;
            try {
                position = carPositions[s];
                rotation = carRotations[s];
            } catch (Exception) {
                position = carPositions[carPositions.Count - 1];
                rotation = carRotations[carRotations.Count - 1];
            }
        }

        public void AddFullData(LapData lap) {
            Time = lap.Time;
            carPositions.AddRange(lap.carPositions);
            carRotations.AddRange(lap.carRotations);
        }

        public void AddNewData(Transform tr) {
            carPositions.Add(tr.position);
            carRotations.Add(tr.rotation);
        }

        public void Reset() {
            carPositions.Clear();
            carRotations.Clear();
        }
    }

}
