﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour {

    [SerializeField] private Image CarsHidPanel, RoadsHidPanel;    
    [SerializeField] private Image CarImage, RoadImage;
    [SerializeField] private Text MaxTurnText, AccelerationText, GripForceText, MaxSpeedText, CarNameText, RoadNameText;

    [SerializeField] private AudioSource ForwardSound, BackWardSound;
    [SerializeField] private AudioSource[] Songs;

    private List<DataManager.CarData> carsData;
    private List<Sprite> carsSprites;
    private int actualCar;

    private List<DataManager.RoadData> roadsData;
    private List<Sprite> roadsSprites;
    private int actualRoad;

    
    /**
     * Lee los datos de los coches y los circuitos y actualiza la UI para que el jugador pueda elegir.
     */
    private void Awake() {
        Debug.Log(Application.dataPath);
        DataManager.LoadMenuData();

        carsData = DataManager.GetCarsData();
        carsSprites = new List<Sprite>();
        foreach (DataManager.CarData ci in carsData) {
            if (ci.imagepath == string.Empty)
                carsSprites.Add(null);
            else
                carsSprites.Add(Resources.Load<Sprite>(ci.imagepath));
        }
        actualCar = 0;
        UpdateCarDataUI();

        roadsData = DataManager.GetRoadsData();
        roadsSprites = new List<Sprite>();
        foreach (DataManager.RoadData ri in roadsData) {
            if (ri.imagepath == string.Empty)
                roadsSprites.Add(null);
            else
                roadsSprites.Add(Resources.Load<Sprite>(ri.imagepath));
        }
        actualRoad = 0;
        UpdateRoadDataUI();

        StartCoroutine(PlayRandomSongs());
    }


    private void UpdateCarDataUI() {
        DataManager.CarData data = carsData[actualCar];
        CarImage.sprite = carsSprites[actualCar];
        MaxTurnText.text = data.maximumsteerangle;
        AccelerationText.text = data.fulltorqueoverallwheels;
        GripForceText.text = data.downforce;
        MaxSpeedText.text = data.topspeed;
        CarNameText.text = data.carname;
    }

    private void UpdateRoadDataUI() {
        DataManager.RoadData data = roadsData[actualRoad];
        RoadImage.sprite = roadsSprites[actualRoad];
        RoadNameText.text = data.roadname;
    }


    /**
     * Al pulsar el boton de Start se guarda el coche seleccionado
     * y se carga la escena correspondiente.
     */
    public void StartGame() {
        PlayerPrefs.SetInt("Car", actualCar);
        SceneManager.LoadScene("Road" + (actualRoad + 1));
    }



    /**
     * Para indicar al jugador el panel de seleccion actual,  
     * hay un panel semitransparente sobre cada panel de seleccion
     * y se activa cuando el jugador pasa el raton por el otro panel de seleccion,
     * asi si pasa el raton sobre los coches, se activara el panel semitransparente
     * de los circuitos.
     */
    public void OnPointerEnterPanel(int i) {
        if (i == 0) { // enter on cars panel
            /*Color c = CarsHidPanel.color;
            c.a = 0f;
            CarsHidPanel.color = c;
            Color c2 = RoadsHidPanel.color;
            c2.a = 0.5f;
            RoadsHidPanel.color = c2;*/
            CarsHidPanel.gameObject.SetActive(false);
            RoadsHidPanel.gameObject.SetActive(true);
        } else {
            /*Color c = RoadsHidPanel.color;
            c.a = 0f;
            RoadsHidPanel.color = c;
            Color c2 = CarsHidPanel.color;
            c2.a = 0.5f;
            CarsHidPanel.color = c2;*/
            RoadsHidPanel.gameObject.SetActive(false);
            CarsHidPanel.gameObject.SetActive(true);
        }
    }
    

    /**
     * 4 botones: 2 left y 2 right para ambas secciones, con sus respectivos sonidos de Forward y Backward
     */
    public void OnClickBtn(int i) {
        if (i < 2) {  //cars
            if (i == 0) {  //left
                actualCar = actualCar == 0 ? carsData.Count - 1 : actualCar - 1;
                BackWardSound.Play();
            } else {  //right
                ForwardSound.Play();
                actualCar = actualCar == carsData.Count - 1 ? 0 : actualCar + 1;
            }
            UpdateCarDataUI();
        } else {  //roads
            if (i == 2) {  //left
                BackWardSound.Play();
                actualRoad = actualRoad == 0 ? roadsData.Count - 1 : actualRoad - 1;
            } else {  //right
                ForwardSound.Play();
                actualRoad = actualRoad == roadsData.Count - 1 ? 0 : actualRoad + 1;
            }
            UpdateRoadDataUI();
        }
    }




    
    /**
     * Reproduce canciones aleatoriamente
     */
    private IEnumerator PlayRandomSongs() {
        while (true) {
            AudioSource song = Songs[Random.Range(0, Songs.Length)];
            float startVolume = song.volume;
            song.volume = 0;
            song.Play();
            while (song.volume <= startVolume) {
                song.volume += Time.deltaTime * 0.025f;
                yield return null;
            }
            yield return new WaitWhile(() => song.isPlaying);
            yield return new WaitForSeconds(1f);
            song.Stop();
        }
    }

}
