﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.Vehicles.Car;

public class GameManager : MonoBehaviour {

    [SerializeField] private CarController Car;
    [SerializeField] private PECCarController PecCar;
    [SerializeField] private int LapsToFinish = 3;

    [SerializeField] private Text TimeText, CountdownText, BestLapTimeText, RecText;
    [SerializeField] private Text[] LapsTimesTexts;

    private Transform carTr;
    private int lapsFinished;
    private float actualLapTime, bestLapTime;

    public static GameManager Instance { get; private set; }
    public static bool CarCanMove { get; private set; }
    public static float SamplesTime { get => Instance.TimeBetweenGhostSamples;  }


    private void Awake() {
        if (Instance) {
            Destroy(Instance);
            Instance = null;
        }
        Instance = this;

        // Cargar los datos correspondientes a este circuito
        DataManager.LoadData(SceneManager.GetActiveScene().name.Equals("Road1"));

        carTr = Car.transform;
        lapsFinished = 0;
        actualLapTime = bestLapTime = 0f;
        CarCanMove = false;

        // Aactualizar el texto de la mejor vuelta si existen datos guardados, sino desactivar el fantasma tambien
        if (!DataManager.IsGhostLapDataEmpty()) {
            carGhost.gameObject.SetActive(true);
            DataManager.LapData ghostLap = DataManager.GhostLapData;
            //BestLapTimeText.text = TimeToString(ghostLap.Time);
            bestLapTime = ghostLap.Time;
            BestLapTimeText.text = TimeToString(ghostLap.Time);
        } else {
            carGhost.gameObject.SetActive(false);
        }

        StartCoroutine(Countdown());
        StartCoroutine(PlayRandomSongs());
    }

    private void Update() {
        // En cualquier momento el jugador puede reiniciar la carrera pulsando R o volver al menu con Esc
        if (Input.GetKeyDown(KeyCode.R)) {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        } else if (Input.GetKeyDown(KeyCode.Escape)) {
            SceneManager.LoadScene("Menu");
        }
    }


    public static void LapFinished() {
        Instance.LapFinishedInstance();
    }

    private void LapFinishedInstance() {
        lapsFinished++;
    }


    private IEnumerator Countdown() {
        yield return new WaitForSeconds(2f);
        float t = 3;
        WaitForSeconds countdownWait = new WaitForSeconds(1f);
        while (t > 0) {
            CountdownText.text = t-- + "";
            CountDownSound.Play();
            yield return countdownWait;
        }
        CountDownSound.volume = 1;
        CountDownSound.pitch = 1.5f;
        CountDownSound.Play();
        CountdownText.text = "GO!";
        StartCoroutine(TimeTextUpdate());
        InitGhost();
        CarCanMove = true;
        yield return countdownWait;
        CountdownText.gameObject.SetActive(false);
        CountdownText.text = string.Empty;
    }


    /**
     * Corutina que actualiza el texto del tiempo,
     * genera los datos del fantasma y
     * comprueba si la vuelta actual ha sido la mejor vuelta y actualiza los datos
     */
    private IEnumerator TimeTextUpdate() {
        // Comprobacionde datos existentes
        if (!DataManager.IsGhostLapDataEmpty()) {
            carGhost.gameObject.SetActive(true);
            DataManager.LapData ghostLap = DataManager.GhostLapData;
            BestLapTimeText.text = TimeToString(ghostLap.Time);
            //bestLapTime = ghostLap.Time;
            //tempActualLapData.Time = ghostLap.Time;
            StartCoroutine(UpdateGhostData(ghostLap));
        } else {
            carGhost.gameObject.SetActive(false);
        }
        // Bucle principal que genera los datos de la vuelta
        int actualLap = lapsFinished;
        float t = 0, samplesTime = 0;
        TimeText.text = t + "";
        while (actualLap == lapsFinished) {
            t += Time.deltaTime;
            samplesTime += Time.deltaTime;
            TimeText.text = TimeToString(t);
            if (samplesTime >= TimeBetweenGhostSamples) {
                samplesTime = 0;
                tempActualLapData.AddNewData(carTr);
            }
            yield return null;
        }
        // Al acabar la vuelta se comprueba con los datos existentes
        // y se pasa a la siguiente vuelta si quedan, sino a la repeticion
        actualLapTime = t;
        LapsTimesTexts[actualLap].text = TimeText.text;
        if (t < bestLapTime || bestLapTime == 0)
            SetBestLapTime(t);
        PecCar.RecordLapData(tempActualLapData);
        tempActualLapData.Reset();
        if (lapsFinished < LapsToFinish)
            StartCoroutine(TimeTextUpdate());
        else
            FinishRace();
    }


    private void SetBestLapTime(float t) {
        bestLapTime = t;
        BestLapTimeText.text = TimeToString(t);
        tempActualLapData.Time = t;
        // Actualiza los datos de la nueva mejor vuelta
        DataManager.SetGhostLapData(tempActualLapData, SceneManager.GetActiveScene().name.Equals("Road1"));
    }


    private string TimeToString(float t) {
        int m = (int)(t / 60);
        t -= m * 60;
        float s = Mathf.Round(t * 1000f) / 1000f;
        string secs = s < 10 ? "0" + s : "" + s;
        string mins = m < 10 ? "0" + m : "" + m;
        if (secs.Length == 3) secs += "000";
        else if (secs.Length == 4) secs += "00";
        else if (secs.Length == 5) secs += "0";
        return mins + ":" + secs.ToString().Replace(',', '.');
    }


    /**
     * Metodo que inicia la repeticion de la carrera
     */
    private void FinishRace() {
        Debug.Log("FINISHED!");
        CarCanMove = false;
        Car.enabled = false;
        carGhost.gameObject.SetActive(false);
        StartCoroutine(RecTextEffect());
        PecCar.StartRepetition();
    }

    /**
     * Efecto intermitente del texto de repeticion
     */
    private IEnumerator RecTextEffect() {
        GameObject recGO = RecText.gameObject;
        recGO.SetActive(true);
        int i = 5;
        while (i >= 0) {
            yield return new WaitForSeconds(0.5f);
            recGO.SetActive(!recGO.activeSelf);
            i--;
        }
        recGO.SetActive(true);
    }




    private void InitGhost() {
        tempActualLapData = new DataManager.LapData();
    }



    //[SerializeField] private GhostLapData GhostData;
    [SerializeField] private float TimeBetweenGhostSamples = 0.2f;
    
    private DataManager.LapData tempActualLapData;
    

    [SerializeField] private Transform carGhost;

    private int samplesCount;
    private Vector3 lastPosition, nextPosition;
    private Quaternion lastRotation, nextRotation;

    /**
     * Corutina que que se encarga de mover el fantasma
     */
    private IEnumerator UpdateGhostData(DataManager.LapData lapData) {
        int actualLap = lapsFinished;
        float samplesTime = 0;
        samplesCount = 0;
        while (actualLap == lapsFinished) {
            samplesTime += Time.deltaTime;
            float pct = samplesTime / TimeBetweenGhostSamples;
            carGhost.position = Vector3.Slerp(lastPosition, nextPosition, pct);
            carGhost.rotation = Quaternion.Slerp(lastRotation, nextRotation, pct);
            if (pct >= 1) {
                samplesTime = 0;
                lastPosition = nextPosition;
                lastRotation = nextRotation;
                lapData.GetDataAt(samplesCount++, out nextPosition, out nextRotation);
            }
            yield return null;
        }
    }




    [SerializeField]
    private AudioSource CountDownSound;

    [SerializeField]
    private AudioSource[] Songs;

    /**
     * Corutina encargada de las canciones.
     */
    private IEnumerator PlayRandomSongs() {
        yield return new WaitForSeconds(4f);
        while (true) {
            yield return new WaitForSeconds(2.5f);
            AudioSource song = Songs[Random.Range(0, Songs.Length)];
            float startVolume = song.volume;
            song.volume = 0;
            song.Play();
            while (song.volume <= startVolume) {
                song.volume += Time.deltaTime * 0.05f;
                yield return null;
            }
            yield return new WaitWhile(() => song.isPlaying);
            song.Stop();
        }
    }
}
