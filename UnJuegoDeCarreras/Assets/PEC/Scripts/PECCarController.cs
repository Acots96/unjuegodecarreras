﻿using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

public class PECCarController : MonoBehaviour {

    [SerializeField] private CinemachineVirtualCamera VCam;
    [SerializeField] private Transform[] CamPosAfterRace;
    [SerializeField] private Transform CamCenter;

    [SerializeField] private CarController car;
    [SerializeField] private Material BodyMat, ComponentsMat, MudsMat, WheelsMat;

    private Transform tr;
    private List<Vector3> positions;
    private List<Quaternion> rotations;

    private int nextCamIdx;
    private bool isOver, isRecording;
    private Vector3 startFollowOffset;


    private void Awake() {
        tr = transform;
        positions = new List<Vector3>();
        rotations = new List<Quaternion>();
        nextCamIdx = 0;
        isOver = isRecording = false;
        Debug.Log(name);
        startFollowOffset = VCam.GetCinemachineComponent<CinemachineOrbitalTransposer>().m_FollowOffset;

        InitDamageSmokePoints();
    }

    private void Start() {
        SetData();
    }

    private void Update() {
        currentSpeed = car.CurrentSpeed;
        if (!isRecording)
            DoSound();
    }
    private void Late() {
        currentSpeed = car.CurrentSpeed;
    }


    public void RecordLapData(DataManager.LapData lapData) {
        positions.AddRange(lapData.carPositions);
        rotations.AddRange(lapData.carRotations);
    }


    public void StartRepetition() {
        isOver = false;
        isRecording = true;
        foreach (GameObject go in damagePointsAfter)
            go.SetActive(false);
        AccelerationSound.pitch = 0;
        StartCoroutine(DoRepetition());
        StartCoroutine(CamerasRecord());
    }

    /**
     * Corutina encargada hacer que el coche se mueva y rote durante la repeticion,
     * con los datos de las 3 vueltas guardadas, indefinidamente hasta que el
     * jugador vuelva al menu.
     */
    private IEnumerator DoRepetition() {
        float totalSamplesTime = GameManager.SamplesTime;
        float samplesTime = 0;
        Vector3 lastPosition = tr.position, nextPosition = positions[0];
        Quaternion lastRotation = tr.rotation, nextRotation = rotations[0];
        int i = 1;
        while (i < positions.Count) { 
            samplesTime += Time.deltaTime;
            float pct = samplesTime / totalSamplesTime;
            tr.position = Vector3.Slerp(lastPosition, nextPosition, pct);
            tr.rotation = Quaternion.Slerp(lastRotation, nextRotation, pct);
            if (pct >= 1) {
                samplesTime = 0;
                lastPosition = nextPosition;
                lastRotation = nextRotation;
                nextPosition = positions[i];
                nextRotation = rotations[i];
                i++;
            }
            yield return null;
        }
        isOver = true;
        yield return null;
        StartRepetition();
    }


    
    /**
     * Corutina encargada de los dos efectos de camaras:
     * - Persecucion
     * - Camaras estaticas.
     * Elige aleatoriamente una de las dos opciones y durante cierto tiempo
     * lo mantiene hasta que vuelve a empezar, asi no es una vez cada una siempre.
     */
    private IEnumerator CamerasRecord() {
        CinemachineOrbitalTransposer ot = VCam.GetCinemachineComponent<CinemachineOrbitalTransposer>();
        while (!isOver) {
            bool doFollow = UnityEngine.Random.Range(0f, 1f) < 0.5f;
            // La persecucion solo actualiza el Follow de la VCam.
            if (doFollow) {                
                Debug.Log("FOLLOWING TARGET");
                float time = UnityEngine.Random.Range(4f, 10f) + Time.time;
                VCam.Follow = CamCenter;
                while (time  > Time.time) {
                    //+CamCenter.Rotate(Vector3.up * Time.deltaTime * 30);
                    yield return null;
                }
            // Las camaras estaticas se van turnando segun la que este mas cerca del coche,
            // siempre comprobando entre la actual y la siguiente del recorrido.
            } else {
                int closestPos = GetClosestCamPosIdx();
                float time = UnityEngine.Random.Range(4f, 10f) + Time.time;
                // Primero encuentra la mas cercana al coche
                VCam.Follow = GetPositionAt(closestPos);
                Debug.Log("FOLLOWING FROM: " + VCam.Follow);
                Vector3 actualPos = GetPositionAt(closestPos).position,
                        nextPos = GetPositionAt(++closestPos).position;
                ot.m_FollowOffset = Vector3.zero;
                while (time > Time.time) {
                    // Cuando la siguiente camara ya esta mas cerca que la actual, actualiza a la siguiente.
                    if (Vector3.Distance(nextPos, tr.position) < Vector3.Distance(actualPos, tr.position)) {
                        VCam.Follow = GetPositionAt(closestPos);
                        actualPos = nextPos;
                        nextPos = GetPositionAt(++closestPos).position;
                    }
                    yield return null;
                }
            }
        }
    }

    /**
     * Metodo para encontrar la camara mas cercana al coche
     */
    private int GetClosestCamPosIdx() {
        int closestIdx = 0;
        Vector3 closestPos = CamPosAfterRace[closestIdx].position;
        Vector3 carPos = tr.position;
        float dist = Vector3.Distance(carPos, closestPos);
        for (int i = 1; i < CamPosAfterRace.Length; i++) {
            Vector3 pos = CamPosAfterRace[i].position;
            float d = Vector3.Distance(carPos, pos);
            if (d < dist) {
                dist = d;
                closestIdx = i;
                closestPos = pos;
            }
        }
        return closestIdx;
    }

    private Transform GetPositionAt(int idx) {
        //if (idx == CamPosAfterRace.Length)
            //idx = 0;
        idx = idx % CamPosAfterRace.Length;
        return CamPosAfterRace[idx];
    }




    /**
     * Metodo que lee los datos del coche seleccionado para
     * actualizar los atributos correspondientes del CarController 
     * y las propiedades de los materiales.
     */
    private void SetData() {
        DataManager.CarData data = DataManager.GetCarsData()[PlayerPrefs.GetInt("Car")];
        //
        try { car.MaximumSteerAngle = float.Parse(data.maximumsteerangle); }
        catch (Exception) { car.MaximumSteerAngle = 20; }
        try { car.FullTorqueOverAllWheels = float.Parse(data.fulltorqueoverallwheels); } 
        catch (Exception) { car.FullTorqueOverAllWheels = 500; }
        try { car.Downforce = float.Parse(data.downforce); } 
        catch (Exception) { car.Downforce = 200; }
        try { car.Topspeed = float.Parse(data.topspeed); } 
        catch (Exception) { car.Topspeed = 500; }
        //
        SetMaterial(data.materials.body, BodyMat);
        SetMaterial(data.materials.components, ComponentsMat);
        SetMaterial(data.materials.muds, MudsMat);
        SetMaterial(data.materials.wheels, WheelsMat);
    }

    private void SetMaterial(DataManager.MaterialData data, Material mat) {
        mat.color = new Color(
            int.Parse(data.color.Substring(0, 2), System.Globalization.NumberStyles.HexNumber) / 256f,
            int.Parse(data.color.Substring(2, 2), System.Globalization.NumberStyles.HexNumber) / 256f,
            int.Parse(data.color.Substring(4, 2), System.Globalization.NumberStyles.HexNumber) / 256f
        );
        mat.SetFloat("_Metallic", float.Parse(data.metallic.Replace('.', ',')));
        mat.SetFloat("_Glossiness", float.Parse(data.smoothness.Replace('.', ',')));
    }



    [Header("Audio")]

    [SerializeField]
    private AudioSource AccelerationSound, DecelerationSound;

    [SerializeField]
    private float AcceleratingStill, AcceleratingMoving;

    private float pitch = 0, movingActualPitchMax = 0.05f;
    private bool wasMoving = false;
    private float currentSpeed;

    /**
     * Metodo encargado de simular el ruido del motor teniendo en cuenta
     * la aceleracion y la velocidad.
     */
    private void DoSound() {
        float speedFraction = Mathf.Min(1, currentSpeed / car.Topspeed + 0.5f);
        if (car.AccelInput > 0) {  //acelerating
            if (currentSpeed > 0.1f) {  //moving
                if (!wasMoving) { //particular case: race start
                    pitch = 1f;
                    wasMoving = true;
                }
                // Al acelerar se incrementa el pitch
                if (pitch < movingActualPitchMax * speedFraction) {  
                    pitch += Time.deltaTime * AcceleratingMoving * (1 / (movingActualPitchMax * 5));
                // Al llegar a un limite relativo, entonces baja de golpe y empieza a subir 
                // hasta un limite relativo mas alto, de este modo se simulan las marchas, aunque habria que pulirlo...
                } else if (movingActualPitchMax < 2.7f * speedFraction) {  
                    pitch *= 0.8f;
                    //movingActualPitchMax *= 1.5f;
                    movingActualPitchMax = Mathf.Min(2.7f * speedFraction, movingActualPitchMax * 1.5f);
                // Finalmente, al llegar al maximo se hace un ruido que parece "temblar"
                } else {
                    pitch = UnityEngine.Random.Range(2.7f * speedFraction, 2.75f * speedFraction);
                }
            } else {  // Para cuando comienza, que esta quieto en la salida pero con el motor rugiendo al querer acelerar.
                if (pitch < 1.2) {
                    pitch += Time.deltaTime * AcceleratingStill;
                } else { // Al llegar al maximo tambien "tiembla"
                    pitch = UnityEngine.Random.Range(0.9f, 1.2f);
                }
            }
        } else {
            if (currentSpeed > 0) {  // Para que disminuya al desacelerar.
                if (pitch > 0) {
                    pitch -= Time.deltaTime * AcceleratingMoving;
                    //pitch = Mathf.Max(0, pitch);
                    if (pitch < movingActualPitchMax)
                        movingActualPitchMax /= 1.5f;
                }
            }
        }
        AccelerationSound.pitch = pitch;
    }



    [Header("Damge")]

    [SerializeField, Tooltip("Equal to 25% of life")]
    private float MaxDamage = 2500000f;
    [SerializeField]
    private float LifeThreshold = 0.8f;

    [SerializeField]
    private Transform DamageSmokePointsParent;

    [SerializeField]
    private AudioSource DamageSound;

    private float life = 1;
    private float damage;
    private int initialAmountOfPoints;
    private List<GameObject> damagePoints, damagePointsAfter;

    /**
     * Efectos visaules de daño.
     * Contiene varios puntos en el coche de los cuales se elige un 
     * porcentaje aleatoriamente.
     */
    private void InitDamageSmokePoints() {
        List<GameObject> points = new List<GameObject>();
        foreach (Transform child in DamageSmokePointsParent)
            points.Add(child.gameObject);
        int amount = (int) (points.Count * UnityEngine.Random.Range(0.5f, 0.8f));
        damagePoints = new List<GameObject>();
        while (amount > 0) {
            GameObject p = points[UnityEngine.Random.Range(0, points.Count)];
            points.Remove(p);
            damagePoints.Add(p);
            amount--;
        }
        damagePointsAfter = new List<GameObject>(damagePoints);
        initialAmountOfPoints = damagePoints.Count;
    }

    /**
     * Al colisionar con algo, se comprueba la cantidad de daño y, en base a eso,
     * se disminuye la velocidad y eligen aleatoriamente una cierta cantidad de puntos de daño
     * para activarlos y que suelten humo.
     */
    private void OnCollisionEnter(Collision collision) {
        if (isRecording)
            return;
        damage = collision.impulse.magnitude / Time.fixedDeltaTime;
        float lifeDamaged = life;
        float dmgFraction = damage / MaxDamage;
        life -= dmgFraction * 0.25f;
        life = Mathf.Max(0, life);
        if (life <= LifeThreshold) {
            int amountPointsToActivate = (int) ((lifeDamaged - life) * initialAmountOfPoints);
            while (damagePoints.Count > 0 && amountPointsToActivate > 0) {
                GameObject p = damagePoints[UnityEngine.Random.Range(0, damagePoints.Count)];
                damagePoints.Remove(p);
                p.SetActive(true);
                amountPointsToActivate--;
            }
        }
        car.Damage = 1 - Mathf.Min(1f, life + 0.35f);
        DamageSound.volume = dmgFraction;        
        DamageSound.Play();
    }

}
