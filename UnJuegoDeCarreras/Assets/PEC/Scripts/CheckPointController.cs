﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointController : MonoBehaviour {

    [SerializeField] private GameObject NextCheckPoint;
    [SerializeField] private bool IsFirst;
    [SerializeField] private LayerMask layersToCheck;


    private void OnTriggerEnter(Collider other) {
        int layer = other.gameObject.layer;
        if (layersToCheck == (layersToCheck | (1 << layer))) {
            if (IsFirst)
                GameManager.LapFinished();
            NextCheckPoint.SetActive(true);
            gameObject.SetActive(false);
        }
    }
}
